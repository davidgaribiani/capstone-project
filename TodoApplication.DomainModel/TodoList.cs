﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TodoApplication.DomainModel
{
    public class TodoList
    {
        [Key]
        [Display(Name = "List ID")]
        public int ID { get; set; }

        [Display(Name = "List Name")]
        [Required(ErrorMessage = "'List Name' can not be empty")]
        [StringLength(30, MinimumLength = 5, ErrorMessage = "Length of 'List Name' can not be less than 5 characters and can not exceed 30 characters")]
        public string ListName { get; set; }

        [Display(Name = "IsHidden")]

        public bool IsHidden { get; set; } = false;

        [Display(Name = "Description")]
        [MaxLengthAttribute(150, ErrorMessage = "Length of 'Description' can not exceed 150 characters")]
        public string Description { get; set; }

        [Display(Name = "TODO Entries")]
        public List<TodoEntry> TodoEntries { get; set; } = new List<TodoEntry>();
    }
}
