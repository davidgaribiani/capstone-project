﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TodoApplication.DomainModel
{
    public class TodoEntry : IValidatableObject
    {
        [Key]
        [Display(Name = "Entry ID")]
        public int ID { get; set; }

        [Display(Name = "Entry Status")]
        public string Status { get; set; }

        [Display(Name = "Entry Title")]
        [Required(ErrorMessage = "'Entry Title' can not be empty.")]
        [StringLength(30, MinimumLength = 5, ErrorMessage = "Length of 'Entry Title' can not be less than 5 characters and can not exceed 30 characters.")]
        public string Title { get; set; }

        [Display(Name = "Description")]
        [MaxLengthAttribute(150, ErrorMessage = "Length of 'Description' can not exceed 150 characters.")]
        public string Description { get; set; }

        [Required(ErrorMessage ="Due Time is required")]
        [Display(Name = "Due Time")]
        [DataType(DataType.Date, ErrorMessage = "Invalid Date/Time.")]
        public DateTime DueTime { get; set; }

        [Display(Name = "Creation Time")]
        public DateTime CreationTime { get; set; } = DateTime.Now;

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            List<ValidationResult> res = new List<ValidationResult>();
            
            if (DateTime.Compare(DateTime.Now, DueTime) > 0)
            {
                ValidationResult mss = new ValidationResult("Due date/time must be greater than or equal to the current date/time");
                res.Add(mss);
            }

            return res;
        }
    }
}
