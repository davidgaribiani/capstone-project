namespace TodoApplication.DomainModel  // TODO Change namespace to '.Models'
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
