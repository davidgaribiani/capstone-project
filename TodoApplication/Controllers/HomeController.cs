﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Data;
using Newtonsoft.Json;
using TodoApplication.DomainModel;
using TodoApplication.Repository;

namespace TodoApplication.Controllers
{
    [Route("/")]
    [ApiController]
    public class HomeController : Controller
    {
        private readonly ITodoListsRepository repository;

        public HomeController(ITodoListsRepository repository)
        {
            this.repository = repository;
        }

        #region "ACTION METHODS RETURNING VIEWS"

        [HttpGet("/")]
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("ShowHidden") == "true")
            {
                return View(repository.TodoLists);
            }
            else
            {
                return View(repository.TodoLists.Where(x => !x.IsHidden));
            }
        }

        [HttpGet("/error/{errorMessage}")]
        public IActionResult Error(string errorMessage)
        {
            ViewBag.Text = errorMessage;

            return View();
        }

        [HttpGet("/error")]
        public IActionResult Error()
        {
            ViewBag.Text = "Something Went Wrong";

            return View();
        }
        
        [HttpGet("/debug")]
        public IActionResult Debug()
        {
            return View(repository);
        }
        #endregion

        #region "GET TODO API"

        [HttpGet("/api/get/entry/{id:int}")]
        public IActionResult GetEntry([FromRoute] int id)
        {
            var entry = repository.GetEntry(id);
            return Json(JsonConvert.SerializeObject(entry));
        }

        [HttpGet("/api/get/list/{id:int}")]
        public IActionResult GetList([FromRoute] int id)
        {
            var list = repository.GetList(id);
            return Json(JsonConvert.SerializeObject(list));
        }

        [HttpGet("/api/get/lists")]
        public IActionResult GetLists()
        {
            var lists = repository.TodoLists;
            return Json(JsonConvert.SerializeObject(lists));
        }

        #endregion

        #region "SUBMIT MODAL HTTP REQUESTS"

        [HttpPut("/modify/list")]
        public IActionResult ModifyList([FromBody] TodoList todoList)
        {
            if (ModelState.IsValid)
            {
                repository.ModifyList(todoList.ID, new TodoList { ListName = todoList.ListName, Description = todoList.Description, IsHidden = todoList.IsHidden });
                return Ok();
            }
            else
            {
                return BadRequest(new { errorMessages = ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage)
                    .ToList() 
                });
            }

        }

        [HttpPut("/modify/entry")]
        public IActionResult ModifyEntry([FromBody] TodoEntry todoEntry)
        {
            if (ModelState.IsValid)
            {
                repository.ModifyEntry(todoEntry.ID, new TodoEntry
                {
                    Title = todoEntry.Title,
                    Description = todoEntry.Description,
                    DueTime = todoEntry.DueTime,
                    Status = todoEntry.Status
                });

                return Ok();
            }
            else
            {
                return BadRequest(new { errorMessages = ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage)
                    .ToList() 
                });
            }
        }

        [HttpPost("/add/list")]
        public IActionResult AddList([FromBody] TodoList todoList)
        {
            if (ModelState.IsValid)
            {
                repository.AddTodoList(todoList);
                return Ok();
            }
            else
            {
                return BadRequest(new { errorMessages = ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage)
                    .ToList() 
                });
            }
        }

        [HttpPost("/add/entry/{id:int}")]
        public IActionResult AddEntry([FromRoute] int id, [FromBody] TodoEntry todoEntry)
        {
            if (ModelState.IsValid)
            {
                repository.AddTodoEntry(id, todoEntry);
                return Ok();
            }
            else
            {
                return BadRequest(new
                {
                    errorMessages = ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage)
                    .ToList()
                });
            }
        }

        [HttpDelete("/delete/entry/{id:int}")]
        public IActionResult DeleteEntry([FromRoute] int id)
        {
            try
            {
                repository.DeleteTodoEntry(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = "Something Went Wrong", errorMessages = ex.Message });
            }
        }

        [HttpDelete("/delete/list/{id:int}")]
        public IActionResult DeleteList([FromRoute] int id)
        {
            try
            {
                repository.DeleteTodoList(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = "Something Went Wrong", errorMessages = ex.Message });
            }
        }

        #endregion

        #region "ACTION METHODS RETURNING PARTIAL VIEWS"

        [HttpGet("/partial/modifyEntry/{id:int}")]
        public PartialViewResult ModifyEntryPartial([FromRoute] int id)
        {
            return PartialView("Modals/_ModifyTodoEntryModal", repository.GetEntry(id));
        }

        [HttpGet("/partial/modifyList/{id:int}")]
        public PartialViewResult ModifyListPartial([FromRoute] int id)
        {
            return PartialView("Modals/_ModifyTodoListModal", repository.GetList(id));
        }

        [HttpGet("/partial/addEntry/{id:int}")]
        public PartialViewResult AddEntryPartial([FromRoute] int id)
        {
            return PartialView("Modals/_AddTodoEntryModal", repository.GetList(id));
        }

        [HttpGet("/partial/addList")]
        public PartialViewResult AddListPartial()
        {
            return PartialView("Modals/_AddTodoListModal", new TodoList { IsHidden = false });
        }

        [HttpGet("/partial/deleteEntry/{id:int}")]
        public PartialViewResult DeleteEntryPartial([FromRoute] int id)
        {
            return PartialView("Modals/_DeleteTodoEntryModal", repository.GetEntry(id));
        }

        [HttpGet("/partial/deleteList/{id:int}")]
        public PartialViewResult DeleteListPartial([FromRoute] int id)
        {
            return PartialView("Modals/_DeleteTodoListModal", repository.GetList(id));
        }

        [HttpGet("/partial/listDetails/{id:int}")]
        public PartialViewResult ListDetailsPartial([FromRoute] int id)
        {
            return PartialView("Modals/_ListDetailsModal", repository.GetList(id));
        }

        [HttpGet("/partial/entryDetails/{id:int}")]
        public PartialViewResult EntryDetailsPartial([FromRoute] int id)
        {
            return PartialView("Modals/_EntryDetailsModal", repository.GetEntry(id));
        }

        #endregion

        #region "OTHER ACTION METHODS"
        
        [HttpPost("/ShowHidden/{value}")]
        public ActionResult SetVariable(string value)
        {
            HttpContext.Session.SetString("ShowHidden", value);
            return this.Json(new { success = true });
        }

        [HttpPut("/statusOnCreate/change/{statusNumber:range(0,2)}")]
        public ActionResult ChangeDefaultStatusOnCreate(int statusNumber)
        {
            repository.ChangeDefaultEntryStatus(statusNumber);

            return this.Json(new { success = true });
        }

        [HttpDelete("/db/empty")]
        public void EmptyDatabase()
        {
            repository.DeleteAllRows();
            RedirectToAction(nameof(Index));
        }

        #endregion
    }
}

