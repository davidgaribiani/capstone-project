﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using TodoApplication.Data;
using TodoApplication.DomainModel;

namespace TodoApplication.Repository
{
    public class EFTodoListsRepository :
        ITodoListsRepository
    {
        private readonly ApplicationDbContext context;

        public EFTodoListsRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<TodoList> TodoLists => context.TodoLists.Include(x => x.TodoEntries);

        public IQueryable<TodoEntry> TodoEntries => context.TodoEntries;

        public IQueryable<TodoList> NotHiddenTodoLists => context.TodoLists.Where(x => !x.IsHidden).Include(x => x.TodoEntries);

        public string GetDefaultEntryStatusOnCreate()
        {
            string value;

            if (File.Exists("todoSettings.txt"))
            {
                var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText("todoSettings.txt"));

                bool hasValue = dictionary.TryGetValue("DefaultEntryStatusOnCreate", out value);

                if (hasValue)
                {
                    return value;
                }
                else
                {
                    return "Not Started";
                }
            }
            else
            {
                return "Not Started";
            }
        }

        public void ChangeDefaultEntryStatus(int entryStatusId)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Clear();
            dictionary.Add("DefaultEntryStatusOnCreate", EnumIdDescriptionToString(entryStatusId));

            File.WriteAllText("todoSettings.txt", JsonConvert.SerializeObject(dictionary, Formatting.Indented));
        }

        public string EnumIdDescriptionToString(int entryStatusId)
        {
            if (entryStatusId < 0 || entryStatusId > 2)
            {
                throw new ArgumentException("Wrong Entry Status ID");
            }
            var value = (EntryStatuses)entryStatusId;
            DescriptionAttribute attribute = value.GetType()
               .GetField(value.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false)
               .SingleOrDefault() as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }

        public TodoEntry GetEntry(int id) => context.TodoEntries.FirstOrDefault(x => x.ID == id);

        public TodoList GetList(int id) => context.TodoLists.Include(x => x.TodoEntries).FirstOrDefault(x => x.ID == id);

        public void AddTodoList(TodoList todoList)
        {
            context.TodoLists.Add(todoList);
            context.SaveChanges();
        }

        public void AddTodoEntry(int listID, TodoEntry todoEntry)
        {
            var todolist = context.TodoLists.Include(inc => inc.TodoEntries).Single(x => x.ID == listID);
            todoEntry.Status = GetDefaultEntryStatusOnCreate();
            todolist.TodoEntries.Add(todoEntry);
            context.SaveChanges();
        }

        public void DeleteTodoList(int id)
        {
            var list = context.TodoLists.Include(inc => inc.TodoEntries).Single(x => x.ID == id);

            foreach (TodoEntry entry in list.TodoEntries)
            {
                context.TodoEntries.Remove(entry);
            }

            context.TodoLists.Remove(list);
            context.SaveChanges();
        }

        public void DeleteTodoEntry(int id)
        {
            var todoEntry = context.TodoEntries.First(x => x.ID == id);
            context.TodoEntries.Remove(todoEntry);
            context.SaveChanges();
        }

        public void DeleteAllRows()
        {
            foreach (TodoEntry todoEntry in context.TodoEntries)
            {
                context.TodoEntries.Remove(todoEntry);
            }

            foreach (TodoList todoList in context.TodoLists)
            {
                context.TodoLists.Remove(todoList);
            }

            context.SaveChanges();
        }

        public void ModifyEntry(int id, TodoEntry todoEntry)
        {
            var entry = context.TodoEntries.Single(x => x.ID == id);

            entry.Title = todoEntry.Title;
            entry.Description = todoEntry.Description;
            entry.DueTime = todoEntry.DueTime;
            entry.Status = todoEntry.Status;

            context.SaveChanges();
        }

        public void ModifyList(int id, TodoList todoList)
        {
            var list = context.TodoLists.Single(x => x.ID == id);
            list.ListName = todoList.ListName;
            list.Description = todoList.Description;
            list.IsHidden = todoList.IsHidden;

            context.SaveChanges();
        }
    }
}
