﻿using System;
using System.ComponentModel;
using System.Linq;
using TodoApplication.DomainModel;

namespace TodoApplication.Repository
{
    public enum EntryStatuses
    {
        [Description("Not Started")]
        NotStarted,
        [Description("In Progress")]
        InProgress,
        [Description("Completed")]
        Completed
    }

    public interface ITodoListsRepository
    {
        void ChangeDefaultEntryStatus(int entryStatusId);

        string GetDefaultEntryStatusOnCreate();

        IQueryable<TodoList> TodoLists { get; }

        IQueryable<TodoList> NotHiddenTodoLists { get; }

        IQueryable<TodoEntry> TodoEntries { get; }

        TodoEntry GetEntry(int id);

        TodoList GetList(int id);

        void AddTodoList(TodoList todoList);

        void DeleteTodoList(int id);

        void AddTodoEntry(int listID, TodoEntry todoEntry);

        void DeleteTodoEntry(int id);

        void DeleteAllRows();

        void ModifyEntry(int id, TodoEntry todoEntry);

        void ModifyList(int id, TodoList todoList);

    }
}
