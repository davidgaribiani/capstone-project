﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using TodoApplication.DomainModel;

namespace TodoApplication.Data
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            ApplicationDbContext context = app.ApplicationServices
                .CreateScope().ServiceProvider.GetRequiredService<ApplicationDbContext>();

            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

            if (!context.TodoLists.Any())
            {
                context.TodoLists.AddRange(
                   new TodoList
                   {
                       ListName = "FirstTodoList",
                       IsHidden = false,
                   },
                   new TodoList
                   {
                       ListName = "SecondTodoList",
                       IsHidden = false
                   },
                   new TodoList
                   {
                       ListName = "ThirdTodoList",
                       IsHidden = false,
                   },
                   new TodoList
                   {
                       ListName = "FourthTodoList",
                       IsHidden = false
                   },
                   new TodoList
                   {
                       ListName = "FifthTodoList",
                       IsHidden = false,
                   });

                context.SaveChanges();

                context.TodoLists.Include(inc => inc.TodoEntries).Single(x => x.ListName == "FirstTodoList").TodoEntries.AddRange(
                        new List<TodoEntry>
                        {
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "clean the house",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 2),
                                CreationTime = DateTime.Now,
                            },
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "walk the dog",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 3),
                                CreationTime = DateTime.Now,
                            },
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "family dinner",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 4),
                                CreationTime = DateTime.Now,
                            },
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "feed the dog",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 4),
                                CreationTime = DateTime.Now,
                            }
                        }
                    );

                context.TodoLists.Include(inc => inc.TodoEntries).Single(x => x.ListName == "SecondTodoList").TodoEntries.AddRange(
                       new List<TodoEntry>
                       {
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "rewatch all marvel series",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 2),
                                CreationTime = DateTime.Now,
                            },
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "rewatch all marvel movies",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 3),
                                CreationTime = DateTime.Now,
                            },
                       }
                   );

                context.TodoLists.Include(inc => inc.TodoEntries).Single(x => x.ListName == "ThirdTodoList").TodoEntries.AddRange(
                       new List<TodoEntry>
                       {
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "trip to Versailles",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 2),
                                CreationTime = DateTime.Now,
                            },
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "trip to Bruges",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 3),
                                CreationTime = DateTime.Now,
                            },
                       }
                   );

                context.TodoLists.Include(inc => inc.TodoEntries).Single(x => x.ListName == "FourthTodoList").TodoEntries.AddRange(
                       new List<TodoEntry>
                       {
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "enjoy Big Tasty",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 2),
                                CreationTime = DateTime.Now,
                            },
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "enjoy Pizza",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 3),
                                CreationTime = DateTime.Now,
                            },
                       }
                   );

                context.TodoLists.Include(inc => inc.TodoEntries).Single(x => x.ListName == "FifthTodoList").TodoEntries.AddRange(
                       new List<TodoEntry>
                       {
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "buy diary products",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 2),
                                CreationTime = DateTime.Now,
                            },
                            new TodoEntry
                            {
                                Description = "....",
                                Title = "buy diary products",
                                Status = "Not Started",
                                DueTime = new DateTime(2025, 1, 3),
                                CreationTime = DateTime.Now,
                            },
                       }
                   );

                context.SaveChanges();
            }
        }
    }
}
