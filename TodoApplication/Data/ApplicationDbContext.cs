﻿using Microsoft.EntityFrameworkCore;
using TodoApplication.DomainModel;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using System;

namespace TodoApplication.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
       : base(options) { }

        public ApplicationDbContext()
        {
            Database.EnsureCreated();
        }

        public DbSet<TodoList> TodoLists { get; set; }

        public DbSet<TodoEntry> TodoEntries { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString("TodoListsConnection"));
        }
    }
}
